package com.arpit.chinnuapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView lot_size, buying_price, total, basic_amount, brokerage_amount, brokerage_received;
    private TextView lot_size_selling, selling_price, total_selling, basic_amount_selling, brokerage_amount_selling, brokerage_received_selling;
    private TextView amount_difference, total_brokerage_received;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lot_size = (TextView) findViewById(R.id.lot_size);
        buying_price = (TextView) findViewById(R.id.buying_price);
        total = (TextView) findViewById(R.id.total);
        basic_amount = (TextView) findViewById(R.id.basic_amount);
        brokerage_amount = (TextView) findViewById(R.id.brokerage_amount);
        brokerage_received = (TextView) findViewById(R.id.brokerage_received);
        lot_size_selling = (TextView) findViewById(R.id.lot_size_selling);
        selling_price = (TextView) findViewById(R.id.selling_price);
        total_selling = (TextView) findViewById(R.id.total_selling);
        basic_amount_selling = (TextView) findViewById(R.id.basic_amount_selling);
        brokerage_amount_selling = (TextView) findViewById(R.id.brokerage_amount_selling);
        brokerage_received_selling = (TextView) findViewById(R.id.brokerage_received_selling);
        amount_difference = (TextView) findViewById(R.id.amount_difference);
        total_brokerage_received = (TextView) findViewById(R.id.total_brokerage_received);

        LinearLayout calculate = (LinearLayout) findViewById(R.id.calculate);
        LinearLayout reset = (LinearLayout) findViewById(R.id.reset);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lot_size.setText("");
                buying_price.setText("");
                lot_size_selling.setText("");
                selling_price.setText("");
            }
        });

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Calculation for Buying Starts from here

                int lotSize = Integer.parseInt(lot_size.getText().toString());
                double buyingPrice = Float.parseFloat(buying_price.getText().toString());
                double totalAmount = lotSize*buyingPrice;
                total.setText(String.format("%.2f", totalAmount)+"");

                int basicAmount = Integer.parseInt(basic_amount.getText().toString());
                int brokerageAmount = Integer.parseInt(brokerage_amount.getText().toString());

                double brokerageReceivedBuying = ((double) brokerageAmount/basicAmount)*totalAmount;
                brokerage_received.setText(String.format("%.2f", brokerageReceivedBuying)+"");

//                Calculation for Selling Starts from here

                int lotSizeSelling = Integer.parseInt(lot_size_selling.getText().toString());
                double sellingPrice = Float.parseFloat(selling_price.getText().toString());
                double totalAmountSelling = lotSizeSelling*sellingPrice;
                total_selling.setText(String.format("%.2f", totalAmountSelling)+"");

                int basicAmountSelling = Integer.parseInt(basic_amount_selling.getText().toString());
                int brokerageAmountSelling = Integer.parseInt(brokerage_amount_selling.getText().toString());

                double brokerageReceivedSelling = ((double) brokerageAmountSelling/basicAmountSelling)*totalAmountSelling;
                brokerage_received_selling.setText(String.format("%.2f", brokerageReceivedSelling)+"");

//                Calculation for Total starts from here

                double totalBrokerage = brokerageReceivedSelling + brokerageReceivedBuying;
                total_brokerage_received.setText(String.format("%.2f", totalBrokerage)+"");

                double difference = totalAmountSelling - totalAmount;
                amount_difference.setText(String.format("%.2f", difference)+"");
            }
        });
    }
}
